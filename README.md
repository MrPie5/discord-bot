Mr Bot 5 is a Discord bot used for Dungeons and Dragons utilities and shitposting written by `Mr Pie 5#0005`, except for the parts that aren't.

Invitation to servers is available on request. Merge requests are welcome.

## Feature requests
Feature requests can be submitted as issues, or otherwise messaged to me in some form.

## Auth
main.py imports an "auth". This is a file named `auth.py` in the same directory as main that contains only the following line:\
`token = "111111111111111111111111111111111111111111111"`\
Replace the 1s with the bot's token that can be found at https://discord.com/developers/applications. This file is not actually included in this repo for obvious reasons.

## Data
The bot stores the ids of servers it has joined and which channels within them are blacklisted or whitelisted through its provided `blacklist` and `whitelist` commands in a (sort of) human-readable json format that is not publicly available. This is the only data the bot currently stores.

## Licensing
I release my code, to the extent that I am able, into the public domain. For more information, see the [license](https://gitlab.com/MrPie5/discord-bot/-/blob/master/LICENSE). Imported libraries, D&D 5e materials, and the Bee Movie script have their own license and are not covered under this.

## Credits
- https://www.freecodecamp.org/news/create-a-discord-bot-with-python/ for a great tutorial and starter code for a Discord bot in Python
- https://norvig.com/lispy.html for code that was used to build the roll parser
- https://towardsdatascience.com/simulating-text-with-markov-chains-in-python-1a27e6d13fc6 for an explanation of how Markov chains work
- Wizards of the Coast for their D&D 5th Edition material
- https://5e.tools/ is a handy D&D 5e reference site which made copying down rules a lot easier
- Obviously I didn't write the Bee Movie script
- Additional credits may be found inline
