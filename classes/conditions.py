class Conditions:

    conditionAliases = {
        "blind" : "blinded",
        "charm" : "charmed",
        "deaf" : "deafened",
        "death" : "dead",
        "exhausted" : "exhaustion",
        "frighten" : "frightened",
        "grapple" : "grappled",
        "incap" : "incapacitated",
        "paralyze" : "paralyzed",
        "petrify" : "petrified",
        "poison" : "poisoned",
        "restrain" : "restrained",
        "stun" : "stunned",
        "surprise" : "surprised",
    }

    conditions = {
    "blinded":"• A blinded creature can't see and automatically fails any ability check that requires sight.\n• Attack rolls against the creature have advantage, and the creature's attack rolls have disadvantage.",
    "charmed":"• A charmed creature can't attack the charmer or target the charmer with harmful abilities or magical effects.\n• The charmer has advantage on any ability check to interact socially with the creature.",
    "dead":"• You're fucking dead, bruh. Don't they teach you this shit in school?",
    "deafened":"• A deafened creature can't hear and automatically fails any ability check that requires hearing.",
    "exhaustion":"pending",
    "frightened":"• A frightened creature has disadvantage on ability checks and attack rolls while the source of its fear is within line of sight.\n• The creature can't willingly move closer to the source of its fear.",
    "grappled":"• A grappled creature's speed becomes 0, and it can't benefit from any bonus to its speed.\n• The condition ends if the grappler is incapacitated.\n• The condition also ends if an effect removes the grappled creature from the reach of the grappler or grappling effect, such as when a creature is hurled away by the thunderwave spell.",
    "incapacitated":"• An incapacitated creature can't take actions or reactions.",
    "invisible":"• An invisible creature is impossible to see without the aid of magic or a special sense. For the purpose of hiding, the creature is heavily obscured. The creature's location can be detected by any noise it makes or any tracks it leaves.\n• Attack rolls against the creature have disadvantage, and the creature's attack rolls have advantage.",
    "paralyzed":"• A paralyzed creature is incapacitated and can't move or speak.\n• The creature automatically fails Strength and Dexterity saving throws.\n• Attack rolls against the creature have advantage.\n• Any attack that hits the creature is a critical hit if the attacker is within 5 feet of the creature.",
    "petrified":"• A petrified creature is transformed, along with any nonmagical object it is wearing or carrying, into a solid inanimate substance (usually stone). Its weight increases by a factor of ten, and it ceases aging.\n• The creature is incapacitated, can't move or speak, and is unaware of its surroundings.\n• Attack rolls against the creature have advantage.\n• The creature automatically fails Strength and Dexterity saving throws.\n• The creature has resistance to all damage.\n• The creature is immune to poison and disease, although a poison or disease already in its system is suspended, not neutralized.",
    "poisoned":"• A poisoned creature has disadvantage on attack rolls and ability checks.",
    "prone":"• A prone creature's only movement option is to crawl, unless it stands up and thereby ends the condition.\n• The creature has disadvantage on attack rolls.\n• An attack roll against the creature has advantage if the attacker is within 5 feet of the creature. Otherwise, the attack roll has disadvantage.",
    "restrained":"• A restrained creature's speed becomes 0, and it can't benefit from any bonus to its speed.\n• Attack rolls against the creature have advantage, and the creature's attack rolls have disadvantage.\n• The creature has disadvantage on Dexterity saving throws.",
    "surprised":"• Any character or monster that doesn’t notice a threat is surprised at the start of the encounter.\n• If you’re surprised, you can’t move or take an action on your first turn of the combat, and you can’t take a Reaction until that turn ends. A member of a group can be surprised even if the other Members aren’t.",
    "stunned":"• A stunned creature is incapacitated, can't move, and can speak only falteringly.\n• The creature automatically fails Strength and Dexterity saving throws.\n• Attack rolls against the creature have advantage.",
    "unconscious":"• An unconscious creature is incapacitated, can't move or speak, and is unaware of its surroundings.\n• The creature drops whatever it's holding and falls prone.\n• The creature automatically fails Strength and Dexterity saving throws.\n• Attack rolls against the creature have advantage.\n• Any attack that hits the creature is a critical hit if the attacker is within 5 feet of the creature."
    }

    def search(self, condition:str) -> str:
        if condition in self.conditionAliases:
            condition = self.conditionAliases[condition]

        if condition in self.conditions:
            return self.conditions[condition]
        else:
            return ""

    def list_conditions(self) -> str:
        output = ""
        for key in self.conditions.keys():
            output = output + key + ", "
        return output.rstrip(", ")

conditions = Conditions()