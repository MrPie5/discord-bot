class Rules:
    ruleAliases = {
        "0 hp" : "dropping to 0 hp",
        "aoe" : "area of effect",
        "area of effect" : "area of effect",
        "carry weight" : "lifting and carrying",
        "carrying capacity" : "lifting and carrying",
        "climb" : "climbing",
        "dismounting" : "mounting and dismounting",
        "death" : "instant death",
        "death saving throw" : "death saving throws",
        "drink" : "water",
        "drinking" : "water",
        "dropping to 0" : "dropping to 0 hp",
        "eat" : "food",
        "eating" : "food",
        "fall" : "falling",
        "grapple" : "grappling",
        "jump" : "jumping",
        "knockout" : "knock-out",
        "knots" : "tying knots",
        "mounted" : "mounted combat",
        "mounting" : "mounting and dismounting",
        "non-lethal" : "knock out",
        "nonlethal" : "knock out",
        "non lethal" : "knock out",
        "shove" : "shoving",
        "spell targeting" : "spell target",
        "suffocate" : "suffocating",
        "suffocation" : "suffocating",
        "stabilized" : "stabilizing",
        "stabilizing a creature" : "stabilizing",
        "swim" : "swimming",
        "three quarters cover" : "three-quarters cover",
        "travel speed" : "travel pace",
        "underwater" : "underwater combat",
    }

    rules = {
        "area of effect" : "A spell's description specifies its area of effect, which typically has one of five different shapes: cone, cube, cylinder, line, or sphere. Every area of effect has a point of origin, a location from which the spell's energy erupts. The rules for each shape specify how you position its point of origin. Typically, a point of origin is a point in space, but some spells have an area whose origin is a creature or an object.\n\nA spell's effect expands in straight lines from the point of origin. If no unblocked straight line extends from the point of origin to a location within the area of effect, that location isn't included in the spell's area. To block one of these imaginary lines, an obstruction must provide total cover.",

        "casting in armor" : "Because of the mental focus and precise gestures required for spellcasting, you must be proficient with the armor you are wearing to cast a spell. You are otherwise too distracted and physically hampered by your armor for spellcasting.",

        "climbing" : "Each foot of movement costs 1 extra foot (2 extra feet in difficult terrain) when you're climbing. You ignore this extra cost if you have a climbing speed and use it to climb.",

        "cone" : "A cone extends in a direction you choose from its point of origin. A cone's width at a given point along its length is equal to that point's distance from the point of origin. A cone's area of effect specifies its maximum length.\n\nA cone's point of origin is not included in the cone's area of effect, unless you decide otherwise.",

        "controlling a mount" : "While you're mounted, you have two options. You can either control the mount or allow it to act independently. Intelligent creatures, such as dragons, act independently.\n\nYou can control a mount only if it has been trained to accept a rider. Domesticated horses, donkeys, and similar creatures are assumed to have such training.\n\nThe initiative of a controlled mount changes to match yours when you mount it. It moves as you direct it, and it has only three action options: Dash, Disengage, and Dodge. A controlled mount can move and act even on the turn that you mount it.\n\nAn independent mount retains its place in the initiative order. Bearing a rider puts no restrictions on the actions the mount can take, and it moves and acts as it wishes. It might flee from combat, rush to attack and devour a badly injured foe, or otherwise act against your wishes.\n\nIn either case, if the mount provokes an opportunity attack while you're on it, the attacker can target you or the mount.",

        "cube" : "You select a cube's point of origin, which lies anywhere on a face of the cubic effect. The cube's size is expressed as the length of each side.\n\nA cube's point of origin is not included in the cube's area of effect, unless you decide otherwise.",

        "cylinder" : "A cylinder's point of origin is the center of a circle of a particular radius, as given in the spell description. The circle must either be on the ground or at the height of the spell effect. The energy in a cylinder expands in straight lines from the point of origin to the perimeter of the circle, forming the base of the cylinder. The spell's effect then shoots up from the base or down from the top, to a distance equal to the height of the cylinder.\n\nA cylinder's point of origin is included in the cylinder's area of effect.",

        "death saving throws" : "Whenever you start your turn with 0 hit points, you must make a special saving throw, called a death saving throw, to determine whether you creep closer to death or hang onto life. Unlike other saving throws, this one isn't tied to any ability score. You are in the hands of fate now, aided only by spells and features that improve your chances of succeeding on a saving throw.\nRoll a d20. If the roll is 10 or higher, you succeed. Otherwise, you fail. A success or failure has no effect by itself. On your third success, you become stable (see below). On your third failure, you die. The successes and failures don't need to be consecutive; keep track of both until you collect three of a kind. The number of both is reset to zero when you regain any hit points or become stable.\n\n**Rolling 1 or 20.** When you make a death saving throw and roll a 1 on the d20, it counts as two failures. If you roll a 20 on the d20, you regain 1 hit point.\n\n**Damage at 0 Hit Points.** If you take any damage while you have 0 hit points, you suffer a death saving throw failure. If the damage is from a critical hit, you suffer two failures instead. If the damage equals or exceeds your hit point maximum, you suffer instant death.",

        "falling" : "At the end of a fall, a creature takes 1d6 bludgeoning damage for every 10 feet it fell, to a maximum of 20d6. The creature lands prone, unless it avoids taking damage from the fall.\nWhen you fall from a great height, you instantly descend up to 500 feet. If you're still falling on your next turn, you descend up to 500 feet at the end of that turn.",

        "food" : "A character needs one pound of food per day and can make food last longer by subsisting on half rations. Eating half a pound of food in a day counts as half a day without food.\n\nA character can go without food for a number of days equal to 3 + his or her Constitution modifier (minimum 1). At the end of each day beyond that limit, a character automatically suffers one level of exhaustion.\n\nA normal day of eating resets the count of days without food to zero.",

        "grappling" : "When you want to grab a creature or wrestle with it, you can use the Attack action to make a special melee attack, a grapple. If you're able to make multiple attacks with the Attack action, this attack replaces one of them.\nThe target of your grapple must be no more than one size larger than you and must be within your reach. Using at least one free hand, you try to seize the target by making a grapple check instead of an attack roll: a Strength (Athletics) check contested by the target's Strength (Athletics) or Dexterity (Acrobatics) check (the target chooses the ability to use). You succeed automatically if the target is incapacitated. If you succeed, you subject the target to the grappled condition (see appendix A). The condition specifies the things that end it, and you can release the target whenever you like (no action required).\n**Escaping a Grapple**\nA grappled creature can use its action to escape. To do so, it must succeed on a Strength (Athletics) or Dexterity (Acrobatics) check contested by your Strength (Athletics) check.\n**Moving a Grappled Creature**\nWhen you move, you can drag or carry the grappled creature with you, but your speed is halved, unless the creature is two or more sizes smaller than you.",

        "half cover" : "A target with **half cover** has a +2 bonus to AC and Dexterity saving throws. A target has half cover if an obstacle blocks at least half of its body. The obstacle might be a low wall, a large piece of furniture, a narrow tree trunk, or a creature, whether that creature is an enemy or a friend.",

        "heavily obscured" : "A heavily obscured area—such as darkness, opaque fog, or dense foliage—blocks vision entirely. A creature effectively suffers from the blinded condition when trying to see something in that area.",

        "high jump" : "When you make a high jump, you leap into the air a number of feet equal to 3 + your Strength modifier (minimum of 0 feet) if you move at least 10 feet on foot immediately before the jump. When you make a standing high jump, you can jump only half that distance. Either way, each foot you clear on the jump costs a foot of movement. In some circumstances, your DM might allow you to make a Strength (Athletics) check to jump higher than you normally can.\nYou can extend your arms half your height above yourself during the jump. Thus, you can reach above you a distance equal to the height of the jump plus 1½ times your height.",

        "instant death" : "Massive damage can kill you instantly. When damage reduces you to 0 hit points and there is damage remaining, you die if the remaining damage equals or exceeds your hit point maximum.\n\nFor example, a cleric with a maximum of 12 hit points currently has 6 hit points. If she takes 18 damage from an attack, she is reduced to 0 hit points, but 12 damage remains. Because the remaining damage equals her hit point maximum, the cleric dies.",

        "knock out" : "Sometimes an attacker wants to incapacitate a foe, rather than deal a killing blow. When an attacker reduces a creature to 0 hit points with a melee attack, the attacker can knock the creature out. The attacker can make this choice the instant the damage is dealt. The creature falls unconscious and is stable.",

        "lifting and carrying" : "Your Strength score determines the amount of weight you can bear. The following terms define what you can lift or carry.\n\n**Carrying Capacity**: Your carrying capacity is your Strength score multiplied by 15. This is the weight (in pounds) that you can carry.\n\n**Push, Drag, or Lift**: You can push, drag, or lift a weight in pounds up to twice your carrying capacity (or 30 times your Strength score). While pushing or dragging weight in excess of your carrying capacity, your speed drops to 5 feet.\n\n**Size and Strength**: Larger creatures can bear more weight, whereas Tiny creatures can carry less. For each size category above Medium, double the creature's carrying capacity and the amount it can push, drag, or lift. For a Tiny creature, halve these weights.",

        "lightly obscured" : "In a lightly obscured area, such as dim light, patchy fog, or moderate foliage, creatures have disadvantage on Wisdom (Perception) checks that rely on sight.",

        "line" : "A line extends from its point of origin in a straight path up to its length and covers an area defined by its width.\n\nA line's point of origin is not included in the line's area of effect, unless you decide otherwise.",

        "long jump" : "When you make a long jump, you cover a number of feet up to your Strength score if you move at least 10 feet on foot immediately before the jump. When you make a standing long jump, you can leap only half that distance. Either way, each foot you clear on the jump costs a foot of movement.\nThis rule assumes that the height of your jump doesn't matter, such as a jump across a stream or chasm. At your DM's option, you must succeed on a DC 10 Strength (Athletics) check to clear a low obstacle (no taller than a quarter of the jump's distance), such as a hedge or low wall. Otherwise, you hit it.\nWhen you land in difficult terrain, you must succeed on a DC 10 Dexterity (Acrobatics) check to land on your feet. Otherwise, you land prone.",

        "long rest" : "A long rest is a period of extended downtime, at least 8 hours long, during which a character sleeps for at least 6 hours and performs no more than 2 hours of light activity, such as reading, talking, eating, or standing watch. If the rest is interrupted by a period of strenuous activity—at least 1 hour of walking, fighting, casting spells, or similar adventuring activity—the characters must begin the rest again to gain any benefit from it.\n\nAt the end of a long rest, a character regains all lost hit points. The character also regains spent Hit Dice, up to a number of dice equal to half of the character's total number of them. You regain at least 1 Hit Die when you finish a long rest.\n\nFor example, if a character has eight Hit Dice, he or she can regain four spent Hit Dice upon finishing a long rest.\n\nA character can't benefit from more than one long rest in a 24-hour period, and a character must have at least 1 hit point at the start of the rest to gain its benefits.",

        "mounting and dismounting" : "Once during your move, you can mount a creature that is within 5 feet of you or dismount. Doing so costs an amount of movement equal to half your speed. For example, if your speed is 30 feet, you must spend 15 feet of movement to mount a horse. Therefore, you can't mount it if you don't have 15 feet of movement left or if your speed is 0.\n\nIf an effect moves your mount against its will while you're on it, you must succeed on a DC 10 Dexterity saving throw or fall off the mount, landing prone in a space within 5 feet of it. If you're knocked prone while mounted, you must make the same saving throw.\n\nIf your mount is knocked prone, you can use your reaction to dismount it as it falls and land on your feet. Otherwise, you are dismounted and fall prone in a space within 5 feet it.",

        "short rest" : "A short rest is a period of downtime, at least 1 hour long, during which a character does nothing more strenuous than eating, drinking, reading, and tending to wounds.\n\nA character can spend one or more Hit Dice at the end of a short rest, up to the character's maximum number of Hit Dice, which is equal to the character's level. For each Hit Die spent in this way, the player rolls the die and adds the character's Constitution modifier to it. The character regains hit points equal to the total (minimum of 0). The player can decide to spend an additional Hit Die after each roll. A character regains some spent Hit Dice upon finishing a long rest, as explained below.",

        "shoving" : "Using the Attack action, you can make a special melee attack to shove a creature, either to knock it prone or push it away from you. If you're able to make multiple attacks with the Attack action, this attack replaces one of them.\n\nThe target of your shove must be no more than one size larger than you, and it must be within your reach. You make a Strength (Athletics) check contested by the target's Strength (Athletics) or Dexterity (Acrobatics) check (the target chooses the ability to use). You succeed automatically if the target is incapacitated. If you succeed, you either knock the target prone or push it 5 feet away from you.",

        "spell target" : "A typical spell requires you to pick one or more targets to be affected by the spell's magic. A spell's description tells you whether the spell targets creatures, objects, or a point of origin for an area of effect.\n\nUnless a spell has a perceptible effect, a creature might not know it was targeted by a spell at all. An effect like crackling lightning is obvious, but a more subtle effect, such as an attempt to read a creature's thoughts, typically goes unnoticed, unless a spell says otherwise.\n\n**A Clear Path to the Target**\nTo target something, you must have a clear path to it, so it can't be behind total cover.\n\nIf you place an area of effect at a point that you can't see and an obstruction, such as a wall, is between you and that point, the point of origin comes into being on the near side of that obstruction.\n\n**Targeting Yourself**\nIf a spell targets a creature of your choice, you can choose yourself, unless the creature must be hostile or specifically a creature other than you. If you are in the area of effect of a spell you cast, you can target yourself.",

        "sphere" : "You select a sphere's point of origin, and the sphere extends outward from that point. The sphere's size is expressed as a radius in feet that extends from the point.\n\nA sphere's point of origin is included in the sphere's area of effect.",

        "suffocating" : "A creature can hold its breath for a number of minutes equal to 1 + its Constitution modifier (minimum of 30 seconds).\n\nWhen a creature runs out of breath or is choking, it can survive for a number of rounds equal to its Constitution modifier (minimum of 1 round). At the start of its next turn, it drops to 0 hit points and is dying, and it can't regain hit points or be stabilized until it can breathe again.\n\nFor example, a creature with a Constitution of 14 can hold its breath for 3 minutes. If it starts suffocating, it has 2 rounds to reach air before it drops to 0 hit points.",

        "stabilizing" : "The best way to save a creature with 0 hit points is to heal it. If healing is unavailable, the creature can at least be stabilized so that it isn't killed by a failed death saving throw.\n\nYou can use your action to administer first aid to an unconscious creature and attempt to stabilize it, which requires a successful DC 10 Wisdom (Medicine) check.\n\nA stable creature doesn't make death saving throws, even though it has 0 hit points, but it does remain unconscious. The creature stops being stable, and must start making death saving throws again, if it takes any damage. A stable creature that isn't healed regains 1 hit point after 1d4 hours.",

        "swimming" : "Each foot of movement costs 1 extra foot (2 extra feet in difficult terrain) when you're swimming. You ignore this extra cost if you have a swimming speed and use it to swim.",

        "three-quarters cover" : "A target with **three-quarters** cover has a +5 bonus to AC and Dexterity saving throws. A target has three-quarters cover if about three-quarters of it is covered by an obstacle. The obstacle might be a portcullis, an arrow slit, or a thick tree trunk.",

        "total cover" : "A target with **total cover** can't be targeted directly by an attack or a spell, although some spells can reach such a target by including it in an area of effect. A target has total cover if it is completely concealed by an obstacle.",

        "tying knots" : "The creature who ties the knot makes an Intelligence (Sleight of Hand) check when doing so. The total of the check becomes the DC for an attempt to untie the knot with an Intelligence (Sleight of Hand) check or to slip out of it with a Dexterity (Acrobatics) check.",

        "underwater combat" : "When making a **melee weapon attack**, a creature that doesn't have a swimming speed (either natural or granted by magic) has disadvantage on the attack roll unless the weapon is a dagger, javelin, shortsword, spear, or trident.\n\nA **ranged weapon attack** automatically misses a target beyond the weapon's normal range. Even against a target within normal range, the attack roll has disadvantage unless the weapon is a crossbow, a net, or a weapon that is thrown like a javelin (including a spear, trident, or dart).\n\nCreatures and objects that are fully immersed in water have resistance to fire damage.",

        "travel pace" : "Fast\t400 feet per minute, 4 miles per hour, 30 miles per day,\t-5 penalty to passive perception\nNormal\t300 feet per minute, 3 miles per hour, 24 miles per day\nSlow\t200 feet per minute, 2 miles per hour, 18 miles per day,\table to use stealth",

        "water" : "A character needs one gallon of water per day, or two gallons per day if the weather is hot. A character who drinks only half that much water must succeed on a DC 15 Constitution saving throw or suffer one level of exhaustion at the end of the day. A character with access to even less water automatically suffers one level of exhaustion at the end of the day.\n\nIf the character already has one or more levels of exhaustion, the character takes two levels in either case.",
    }


    def __init__(self):

        # these are compound rules that include rules from the above dictionary, to avoid duplication
        # since Python dictionaries can't be self-referential, we need another
        rulesAdd = {
            "cover" : "Walls, trees, creatures, and other obstacles can provide cover during combat, making a target more difficult to harm. A target can benefit from cover only when an attack or other effect originates on the opposite side of the cover.\n\nThere are three degrees of cover. If a target is behind multiple sources of cover, only the most protective degree of cover applies; the degrees aren't added together. For example, if a target is behind a creature that gives half cover and a tree trunk that gives three-quarters cover, the target has three-quarters cover.\n\n" + self.rules["half cover"] + self.rules["three-quarters cover"] + self.rules["total cover"],

            "dropping to 0 hp" : "When you drop to 0 hit points, you either die outright or fall unconscious, as explained in the following sections.\n\n**Instant Death**\n" + self.rules["instant death"] + "\n\n**Falling Unconscious**\nIf damage reduces you to 0 hit points and fails to kill you, you fall unconscious (see the condition). This unconsciousness ends if you regain any hit points.\n\n**Death Saving Throws**\n" + self.rules["death saving throws"] + "\n\n**Stabilizing a Creature**\n" + self.rules["stabilizing"],

            "jumping" : "Your Strength determines how far you can jump.\n\n**Long Jump**\n" + self.rules["long jump"] + "\n\n**High Jump**\n" + self.rules["high jump"],

            "mounted combat" : "A knight charging into battle on a warhorse, a wizard casting spells from the back of a griffon, or a cleric soaring through the sky on a pegasus all enjoy the benefits of speed and mobility that a mount can provide.\n\nA willing creature that is at least one size larger than you and that has an appropriate anatomy can serve as a mount, using the following rules.\n\n**Mounting and Dismounting**\n" + self.rules["mounting and dismounting"] + "\n\n**Controlling a Mount**\n" + self.rules["controlling a mount"],

            "obscured" : "A given area might be lightly or heavily obscured.\n\n**Lightly Obscured**\n" + self.rules["lightly obscured"] + "\n\n**Heavily Obscured**\n" + self.rules["heavily obscured"],
        }
        # add rulesAdd into rules
        self.rules.update(rulesAdd)

    def search(self, rule:str) -> str:
        if rule in self.ruleAliases:
            rule = self.ruleAliases[rule]

        if rule in self.rules:
            return self.rules[rule]
        else:
            return ""

    def list_rules(self) -> str:
        output = ""
        for key in sorted(self.rules.keys()):
            output = output + key + ", "
        return output.rstrip(", ")
    

rules = Rules()