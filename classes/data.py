import json

class Data:
    servers = {}

    # structure of the servers object:
    # {
    #   server1ID(int) :
    #       {
    #           "whitelist" : [channel ids (ints)],
    #           "blacklist" : [channel ids (ints)],
    #       },
    #   server2ID(int) : ...
    # }
    # whitelist and blacklist we want to preserve through restarts, so changes to it call the save function

    def initialize_server(self, serverID:int):
        """Initialize the server with all data parameters, or update missing parameters"""

        # check if the server is already registered in data, and if not, initialize it
        if not serverID in self.servers:
            self.servers[serverID] = {}
            self.servers[serverID]["whitelist"] = []
            self.servers[serverID]["blacklist"] = []
        
        # otherwise check every parameter
        else:
            if not "whitelist" in self.servers[serverID]:
                self.servers[serverID]["whitelist"] = []
            if not "blacklist" in self.servers[serverID]:
                self.servers[serverID]["blacklist"] = []

        self.save()

    def can_send_to_channel(self, serverID:int, channelID:int) -> bool:
        if (self.has_whitelist(serverID) and not channelID in self.servers[serverID]["whitelist"]) or (self.in_blacklist(serverID, channelID)):
            return False
        else:
            return True

    def has_whitelist(self, serverID:int) -> bool:
        if (serverID in self.servers) and (len(self.servers[serverID]["whitelist"]) > 0):
            return True
        else:
            return False

    def in_blacklist(self, serverID:int, channelID:int) -> bool:
        if (serverID in self.servers) and (channelID in self.servers[serverID]["blacklist"]):
            return True
        else:
            return False

    def whitelist(self, serverID:int, channelID:int) -> bool:
        if channelID in self.servers[serverID]["whitelist"]:
            self.servers[serverID]["whitelist"].remove(channelID)
            self.save()
            return False
        else:
            self.servers[serverID]["whitelist"].append(channelID)
            self.save()
            return True

    def blacklist(self, serverID:int, channelID:int) -> bool:
        if channelID in self.servers[serverID]["blacklist"]:
            self.servers[serverID]["blacklist"].remove(channelID)
            self.save()
            return False
        else:
            self.servers[serverID]["blacklist"].append(channelID)
            self.save()
            return True

    def clear_whitelist(self, serverID:int):
        if serverID in self.servers:
            self.servers[serverID]["whitelist"] = []
            self.save()

    def clear_blacklist(self, serverID:int):
        if serverID in self.servers:
            self.servers[serverID]["blacklist"] = []
            self.save()

    def save(self) -> bool:
        try:
            with open("data.json", "w") as f:
                f.write(json.dumps(self.servers))
            return True
        except Exception as e:
            print(f"ERROR: could not save data. Exception: {e}")
            return False

    def load(self) -> bool:
        def jsonKeys2int(x):
            if isinstance(x, dict):
                # json assumes all keys are strings
                # this hook means that if the keys can be ints, convert them as such
                try:
                    return {int(k):v for k,v in x.items()}
                except ValueError:
                    pass
            return x

        try:
            with open("data.json", "r") as f:
                self.servers = json.loads(f.read(), object_hook=jsonKeys2int)
            return True
        except Exception as e:
            print(f"ERROR: could not load data. Exception: {e}")
            return False

data = Data()