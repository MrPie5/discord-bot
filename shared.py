import discord

# local imports
from classes.data import data

prefix = "$"

def can_send(ctx:discord.ext.commands.Context):
    return (not isinstance(ctx.channel, discord.TextChannel)) or data.can_send_to_channel(ctx.guild.id, ctx.channel.id)

async def send_long(ctx:discord.ext.commands.Context, message:str):
    """Split a long message into chunks and send them one at a time"""
    i = 0
    while True:
        currentChunk = message[i:i+2000]

        # if the length of the chunk created by our slicing is 0, it means the previous chunk was the last one and we're done
        if len(currentChunk) == 0:
            break
        
        # if the message is at the limit and it's not the last message,
        # we'll attempt to break it at a line break or space rather than an abitrary point
        if len(currentChunk) == 2000 and (len(message) - (i+2000) > 0):
            j = 2000

            # if the chunk has a line break, set j to the index of the last one
            if (x := currentChunk.rfind("\n")) > -1:
                j = x
            # otherwise if it has a space, set j to the index of the last one
            elif (x := currentChunk.rfind(" ")) > -1:
                j = x
        
            currentChunk = currentChunk[0:j]
            await ctx.send(currentChunk)

            # adjust i by the amount we've offset so that we don't lose any content in the next chunk
            i = i - (2000 - j)
        else:
            await ctx.send(currentChunk)
        i = i + 2000