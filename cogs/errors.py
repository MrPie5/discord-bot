import discord

from discord.ext import commands

# local imports
from shared import *

class Errors_Cog(commands.Cog, name="Errors"):
    def __init__(self, bot):
        self.bot = bot

    @commands.Cog.listener()
    async def on_ready(self):
        print("Errors cog ready")

    @commands.Cog.listener()
    async def on_command_error(self, ctx, error):
        error = getattr(error, 'original', error)
        if isinstance(error, commands.CommandNotFound):
            return

        # apply reactions to the offending message as a sort of error code.
        # the try-excepts are in case the bot lacks permissions to apply reactions, in which case do nothing

        if isinstance(error, discord.errors.Forbidden):
            # bot lacks the permission to send messages in this channel
            try:
                await ctx.message.add_reaction("❗")
                await ctx.message.add_reaction("🙊")
            except:
                pass

        elif isinstance(error, discord.ext.commands.errors.NotOwner):
            # user tried to use an owner-restricted command
            try:
                await ctx.message.add_reaction("❗")
                await ctx.message.add_reaction("🙉")
            except:
                pass

        elif isinstance(error, discord.ext.commands.errors.CheckFailure):
            # one of our checks (currently only can_send) failed
            try:
                await ctx.message.add_reaction("❗")
                await ctx.message.add_reaction("📋")
            except:
                pass

        else:
            # unexpected error
            try:
                await ctx.message.add_reaction("❗")
                await ctx.message.add_reaction("<:panik:755197362716147883>")
            except:
                pass
            finally:
                raise error

    @commands.command(name="errors", hidden=True)
    @commands.check(can_send)
    async def list_errors(self, ctx):
        """List error codes"""
        embedVar = discord.Embed(title="Error codes", description="", color=0xff0000)
        embedVar.add_field(name="🙊", value="The bot does not have permission to send messages in this channel.", inline=False)
        embedVar.add_field(name="🙉", value="This command can only be used by the bot's owner.", inline=False)
        embedVar.add_field(name="📋", value="This channel is blacklisted, or not whitelisted.", inline=False)
        embedVar.add_field(name="<:panik:755197362716147883>", value="An unexpected error occurred. Please report this.", inline=False)
        await ctx.send(embed=embedVar)

def setup(bot):
    bot.add_cog(Errors_Cog(bot))