from random import choice, randint

from discord.ext import commands

# local imports
from shared import *

class Eightball:
    responses = {
        "yes" : [
            "Yes.",
            "Yes - definitely.",
            "Yeah.",
            "Ye.",
            "It is certain.",
            "It would seem so.",
            "As I see it, yes.",
            "It is decidedly so.",
            "Most likely.",
            "Outlook good.",
            "Signs point to yes.",
            "Without a doubt.",
            "You may rely on it.",
            "I'll allow it.",
            "Hell... yeah.",
            "So it shall be.",
            "Does an owlbear shit in the woods?",
            "Is the pope Catholic?",
        ],

        "no" : [
            "No.",
            "Nah.",
            "Nah bro.",
            "Oh hell nah.",
            "Don't count on it.",
            "My reply is no.",
            "My sources say no",
            "Outlook not so good.",
            "Very doubtful.",
            "Not a chance.",
            "Chances are not good.",
            "Unlikely.",
            "Don't bet on it.",
            "Is True Strike a good spell?",
            "Sodium bromate.",
            "Oh no, definitely not.",
            "No. No no no no. No.",
        ],

        "other" : [
            "Try asking again.",
            "Ask again later.",
            "Better not tell you now.",
            "Cannot predict now.",
            "Concentrate and ask again.",
            "Reply hazy, try again.",
            "Who are you? Get out of my house.",
            "Does the pope shit in the woods?",
            "How the fuck should I know?",
            "I don't know, maybe, I guess?",
            "It could go either way.",
            "You can't just ask that.",
            "I'm an 8-ball, not a \"deal with your shit\"-ball.",
        ]
    }

    def respond(self):
        # choose a response based on the odds of a real 8ball
        chance = randint(1,20)
        if chance < 11:
            return choice(self.responses["yes"])
        elif chance < 16:
            return choice(self.responses["no"])
        else:
            return choice(self.responses["other"])

eightball = Eightball()

class Eightball_Cog(commands.Cog, name="Eightball"):
    def __init__(self, bot):
        self.bot = bot

    @commands.Cog.listener()
    async def on_ready(self):
        print("Eightball cog ready")

    @commands.command(name="8ball", aliases=["eightball"])
    @commands.check(can_send)
    async def send_8ball(self, ctx, *, arg=None):
        """Ask a question and get a response from the magic eightball"""

        # 1 in 200 chance of an easter egg where the bot asks the 8ball
        if randint(1,200) == 1:
            # Yes, it would be more efficient to do these all in one message, but I think it's funnier to actually do 3
            await ctx.send("I don't know, let me ask.")
            await ctx.send(prefix + "8ball " + arg)
            await ctx.send(eightball.respond())
        else:
            await ctx.send(eightball.respond())

def setup(bot):
    bot.add_cog(Eightball_Cog(bot))