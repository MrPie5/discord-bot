import discord
from random import choice

from discord.ext import commands

# local imports
from resources.wwjd import wwjd
from shared import *

class Shitpost(commands.Cog, name="shitpost"):
    def __init__(self, bot):
        self.bot = bot

    @commands.Cog.listener()
    async def on_ready(self):
        print("Shitpost cog ready")
    
    @commands.command(name="beemovie")
    @commands.check(can_send)
    async def send_beemovie(self, ctx):
        """What do you think this does? You must have the "manage messages" permission in this channel to use."""
        # if this is a text channel (not a DM), the sender needs manage_messages permission
        if isinstance(ctx.channel, discord.TextChannel) and not ctx.author.permissions_in(ctx.channel).manage_messages:
            await ctx.send("This command requires you to have the \"manage messages\" permission")
        else:
            with open("resources/beemovie.txt", "r") as f:
                beemovie = f.read()
                await send_long(ctx, beemovie)

    @commands.command(name="lemon", aliases=["lemons"])
    @commands.check(can_send)
    async def send_lemon(self, ctx, *, arg="lemon"):
        """Paste the lemon-stealing whores copypasta, with "lemon" replaced with the argument"""
        # I stole this idea from Luc's Porygon2 bot in the PMD server
        await ctx.send(
            "I’m so glad that our {0} tree finally grew and sprouted fruitful {0}y {0}s. I mean, imagine, we can make {0}ade, key {0} pie, {0} merengue pie. I think it’s the most valuable of property that we have. I think we should go to the bank and get a loan, actually I think we should just get {0} tree insurance and then get a loan and use the {0} tree as collateral because it is now insured. I truly do love our {0} tree. Just imagine a life full of {0} trees, and all our beautiful {0}s, endless possibilities. They’re so beautiful, I wish I was a {0}. You wish you were a {0}? If you were a {0} I would put you on my shelf and cherish you like I cherish all our {0}s. That’s so beautiful, like I only hope that the whores aren’t stealing our {0}s you know those naughty whores always steal {0}s. We do have a couple {0} whores in this community, those damn {0}-stealing whores I hate them no one will take our prized {0}s from us. Hey, has it been about 10 seconds since we looked at our {0} tree? It has been about 10 seconds till we looked at our {0} tree. Hey what the FUCK".format(arg[0:39])
            # trim at 39 characters because this is the most we can do without going over the character limit for the message
        )

    @commands.command(name="wwjd")
    @commands.check(can_send)
    async def send_wwjd(self, ctx):
        """What would Jesus do? A random selection from a curated list of verses related to Jesus, many of which sound weird out of context."""
        await ctx.send(choice(wwjd))

    @commands.command(name="wwrd")
    @commands.check(can_send)
    async def send_wwrd(self, ctx):
        """What would Richard do? The same as wwjd but with "Jesus" replaced with "Richard\""""
        await ctx.send(choice(wwjd).replace("Jesus", "Richard"))

    @commands.command(name="trick", aliases=["cutbackdropturn", "cbdt"])
    @commands.check(can_send)
    async def send_trick(self, ctx):
        """Do a cut back turn drop cut turn back cut cut back turn cut drop"""
        def phrase(trick):
            return choice([
                "I did it! I did a " + trick + "!",
                "A " + trick + ", huh?",
                "Woah, did he just do a " + trick + "?!",
                "He did a " + trick + ", now that's continuity.",
                "He did a " + trick + ", and it's on fire!",
            ])
        words = ["Cut", "Back", "Drop", "Turn"]
        trick = choice(words) + " " + choice(words) + " " + choice(words) + " " + choice(words)
        await ctx.send(phrase(trick))

def setup(bot):
    bot.add_cog(Shitpost(bot))