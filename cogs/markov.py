from discord.ext import commands

from random import choice, randint
import re

# local imports
from shared import *

class Markov:

    def __init__(self, source, max=3, min=1, singleLine=False):
        self.source = source
        self.singleLine = singleLine
        self.MAX_SEQUENCE_LENGTH = max
        self.MIN_SEQUENCE_LENGTH = min
        self.chains = {}
        self.starts = {}
        self.keysLower = {}
        self.setupDone = False

        with open("resources/" + source, "r", encoding="utf-8") as sourceFile:

            print(f"Generating {source} model with range of {self.MAX_SEQUENCE_LENGTH} to {self.MIN_SEQUENCE_LENGTH}")
            self.teach(sourceFile.read())

        try:
            with open("resources/" + source + "-addons", "r", encoding="utf-8") as sourceFile:
                print(f"Generating addons for {source}")
                self.teach(sourceFile.read())
        except FileNotFoundError:
            pass
        
        self.setupDone = True

        # print("Writing dictionary to file")
        # with open("markoved.txt", "w", encoding="utf-8") as output:
        #     output.write(str(self.chains))

        # print("Writing lower dictionary to file")
        # with open("markoved-lower.txt", "w", encoding="utf-8") as output:
        #     output.write(str(self.keysLower))

        # print("Writing starts to file")
        # with open("markoved-starts.txt" ,"w", encoding="utf-8") as output:
        #     output.write(str(self.starts))

    def teach(self, text):

        if(self.setupDone):
            with open("resources/" + self.source + "-addons", "a") as output:
                output.write("\n" + text)

        if self.singleLine:
            text = text.replace("\n", " ")

        for line in text.split("\n"):
            # split the line file into an array of words, delimited by spaces and newline
            words = re.split(" |\t", re.sub("“|”", '\"', re.sub('‘|’', '\'', line)))

            for sequenceLength in range(self.MAX_SEQUENCE_LENGTH, self.MIN_SEQUENCE_LENGTH-1, -1):

                if not sequenceLength in self.chains:
                    self.chains[sequenceLength] = {}
                if not sequenceLength in self.starts:
                    self.starts[sequenceLength] = []
                start = 0

                for i in range(0, len(words)-sequenceLength):
                    # our first word is our "key", and is a single string made up of {sequenceLength} words
                    word1 = " ".join(words[i:(i+sequenceLength)]).strip("\"'() ")
                    # our second words is our "value", and is the word that immediately follows the above {sequenceLength} words
                    word2 = words[i+sequenceLength].strip("\"'() ")

                    # if either word is an empty string, pass
                    if(len(word1) > 0 and len(word2) > 0):

                        if word1 in self.chains[sequenceLength]:
                            # if our key is already in our chains dictionary, add the value to its list of values
                            self.chains[sequenceLength][word1].append(word2)
                        else:
                            # otherwise initialize the key with word2 as the first value in its list
                            self.chains[sequenceLength][word1] = [word2]

                        if not(word1.islower() or word1.lower() in self.keysLower):
                            # if our key has an uppercase letter, store a map between its lower and normal case variant
                            # for case insensitive searching later
                            self.keysLower[word1.lower()] = word1

                        # This section helps us list how sentences start, which should help make more natural sentences

                        # if start is 0, it means the end of a sentence was sequenceLength words ago, so add the current word (which is of length sequenceLength)
                        if(start == 0):
                            self.starts[sequenceLength].append(word1)
                        
                        if(start >= 0):
                            start -= 1

                        # if we've found the end of a sentence, set start to sequenceLength-1, so one we traverse sequenceLength more words we'll be at the start of a new sentence
                        if(word1[-1]) in ['.', '!', '?']:
                            start = sequenceLength-1

    def regenerate(self):
        self.__init__(self.source, max=self.MAX_SEQUENCE_LENGTH, min=self.MIN_SEQUENCE_LENGTH, singleLine=self.singleLine)

    def sentence(self, sequenceLength=3, minLength=0, maxLength=2000, seed=None):
        sequenceLength = max(min(sequenceLength, self.MAX_SEQUENCE_LENGTH), self.MIN_SEQUENCE_LENGTH)
        sentence = ""

        # if minLength was not given, randomly determine the minimum word length of the sentence
        if not minLength:
            minLength = randint(3, 20)

        if seed:
            sentence = seed
        else:
            sentence = choice(list(self.starts[sequenceLength]))

        sentence = sentence.split(" ")

        # loop until we find an ending punctuation mark while the sentence is over the minLength
        while (sentence[-1][-1] not in [".", "!", "?"] or len(sentence) < minLength):
            toAdd = self.next(sentence[-sequenceLength:])
            if not toAdd:
                break

            sentence.append(toAdd)

        if " ".join(sentence) == seed:
            # no chaining could be done
            return False
        else:
            # capitalize the first letter of the sentence
            sentence[0] = sentence[0].capitalize()
            sentence = " ".join(sentence)

            # if there is an odd number of quotation marks in the sentence, remove one
            # disabled for now because this can make things worse if there's more than one and it removes the quote from a single word, such as "girl"
            # if sentence.count("\"") % 2 == 1:
            #     sentence = sentence.replace("\"", "", 1)

            return sentence

    def next(self, sentence):
        # cap the amount of the sentencce we're going to use at our max length
        searchLength = min(len(sentence), self.MAX_SEQUENCE_LENGTH)

        toAdd = ""

        # do a reverse factorial search through our dictionaries
        # e.g. search for the last 5 words, if that fails, try the last 4, etc. down to the min length
        while searchLength >= self.MIN_SEQUENCE_LENGTH:
            searchTerm = " ".join(sentence[-searchLength:])
            try:
                # take the last {searchLength} words of our sentence, join them into one string, then use that string as a key
                # for our chains dictionary and randomly pick one of its values as the next word in our sentence
                toAdd = choice(self.chains[searchLength][searchTerm])
                break
            except KeyError:
                # if the key can't be found, check if there's a key in a different case, and if so, repeat the search with that
                if searchTerm.lower() in self.keysLower:
                    searchTerm = self.keysLower[searchTerm.lower()]
                    try:
                        toAdd = choice(self.chains[searchLength][searchTerm])
                        break
                    except KeyError:
                        # if we error again then something has gone wrong. Probably a bad seed.
                        pass
            searchLength -= 1

        return toAdd

bibleMarkov = Markov("bible.txt", max=3, singleLine=True)
shitpostMarkov = Markov("shitpost.txt", max=2)
flirtMarkov = Markov("flirt.txt", max=2)

class Markov_Cog(commands.Cog, name="Markov"):
    def __init__(self, bot):
        self.bot = bot

    @commands.Cog.listener()
    async def on_ready(self):
        print("Markov cog ready")

    @commands.command(name="biblemarkov", aliases=["bibleMarkov", "bible"])
    @commands.check(can_send)
    async def send_bibleMarkov(self, ctx, *, seed=None):
        """Generate a markov string from the Bible (American King James Version)"""
        output = bibleMarkov.sentence(seed=seed)
        if output:
            await send_long(ctx, output)
        else:
            await ctx.send("Could not generate a markov from this seed")

    @commands.command(name="shitpostmarkov", aliases=["shitpost", "copypastamarkov", "copypasta"])
    @commands.check(can_send)
    async def send_shitpostMarkov(self, ctx, *, seed=None):
        """Generate a markov string from a selection of copypastas and shitposts"""
        output = shitpostMarkov.sentence(seed=seed)
        if output:
            await send_long(ctx, output)
        else:
            await ctx.send("Could not generate a markov from this seed")

    @commands.command(name="flirtmarkov", aliases=["flirt"])
    @commands.check(can_send)
    async def send_flirtMarkov(self, ctx, *, seed=None):
        """Generate a markov string from a list of pickup lines"""
        output = flirtMarkov.sentence(seed=seed)
        if output:
            await send_long(ctx, output)
        else:
            await ctx.send("Could not generate a markov from this seed")

    @commands.command(name="regenerate", hidden=True)
    @commands.is_owner()
    @commands.check(can_send)
    async def regenerate_Markov(self, ctx, target):
        """Regenerate a markov model"""
        await ctx.message.add_reaction("⌛")
        try:
            model = {
                "shitpost":shitpostMarkov,
                "bible":bibleMarkov,
                "flirt":flirtMarkov,
            }[target]
            model.regenerate()
            await ctx.message.add_reaction("✅")
        except KeyError:
            await ctx.message.add_reaction("🚫")
            await ctx.send("Invalid target")

    @commands.command(name="teach", hidden=True)
    @commands.is_owner()
    @commands.check(can_send)
    async def teach_Markov(self, ctx, *, text):
        """Teach the shitpost model"""
        shitpostMarkov.teach(text)
        await ctx.send("I'm learning")


def setup(bot):
    bot.add_cog(Markov_Cog(bot))