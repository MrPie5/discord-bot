import discord
import math

from discord.ext import commands

# local imports
from shared import *

class Meta_Cog(commands.Cog, name="Meta"):
    def __init__(self, bot):
        self.bot = bot

    @commands.Cog.listener()
    async def on_ready(self):
        print("Meta cog ready")

    @commands.command(name="ping")
    @commands.check(can_send)
    async def ping(self, ctx):
        await ctx.send(f"pong (took {math.floor(self.bot.latency * 1000)} ms)")

    @commands.command(name="about", aliases=["info"])
    @commands.check(can_send)
    async def send_about(self, ctx):
        """Info about the bot"""
        embedVar = discord.Embed(title="About Mr Bot 5", color=0x00ff00, description=
            "Mr Bot 5 is a D&D utility and shitposting bot made by `Mr Pie 5#0005` (except for the parts he stole from other people). Invitation to servers is available on request."
            +"\n\nSource code is available at https://gitlab.com/MrPie5/discord-bot"
        )
        await ctx.send(embed=embedVar)

    # @commands.command(name="commands")
    # @commands.check(can_send)
    # async def send_commands(self, ctx):
    #     """List the bot's commands"""
    #     embedVar = discord.Embed(title="Commands", description="", color=0x00ff00)
    #     embedVar.add_field(name=prefix+"tip", value="Get a random loading screen tip. Note: these are not the \"sarcastic loading tips\" used in my games, those are exclusive.", inline=False)
    #     embedVar.add_field(name=prefix+"roll", value="Roll some dice. This mostly follows Roll20's syntax", inline=False)
    #     embedVar.add_field(name=prefix+"surge, " + prefix+"wildmagic", value="Roll on the Wild Magic Surge table", inline=False)
    #     embedVar.add_field(name=prefix+"netlibram, " + prefix+"surgenl", value="Roll on the Net Libram of Random Magic Effects", inline=False)
    #     embedVar.add_field(name=prefix+"stats", value="Roll a stat array", inline=False)
    #     embedVar.add_field(name=prefix+"condition x", value="Get the definition for condition x. Use `" + prefix+"list conditions` to see all options", inline=False)
    #     embedVar.add_field(name=prefix+"rule x", value="Get the definition for rule x. Use `" + prefix+"list rules` to see all options", inline=False)
    #     embedVar.add_field(name=prefix+"feat x", value="Get the definition for feat x. Use `" + prefix+"list feats` to see all options", inline=False)
    #     embedVar.add_field(name=prefix+"8ball x", value="Ask a question and get a reading from the magic 8ball", inline=False)
    #     embedVar.add_field(name=prefix+"wtf", value="Who the fuck is my D&D character?", inline=False)
    #     embedVar.add_field(name=prefix+"beemovie", value="Paste the Bee Movie script. Requires you to have the manage messages permission", inline=False)    
    #     embedVar.add_field(name=prefix+"lemon x", value="Paste the Lemon Stealing Whores copypasta, with all instances of \"lemon\" replaced with x (39 character limit)", inline=False)
    #     embedVar.add_field(name=prefix+"wwjd", value="Out of context bible quotes from or about Jesus, or an occasional meme. `"+ prefix+"wwrd` does the same but replaces \"Jesus\" with \"Richard\"", inline=False)
    #     embedVar.add_field(name=prefix+"whitelist", value="I'll add this channel to my whitelist, and ignore most commands in channels not in the whitelist. Use again to remove.", inline=False)
    #     embedVar.add_field(name=prefix+"blacklist", value="I'll add this channel to my blacklist, and ignore most commands in channels in the blacklist. Use again to remove.", inline=False)
    #     embedVar.add_field(name=prefix+"commands", value="Display this list of commands", inline=False)
    #     embedVar.add_field(name=prefix+"about, " + prefix+"info", value="What is my purpose?", inline=False)
    #     await ctx.send(embed=embedVar)


    # These commands ignore the blacklist and whitelist

    @commands.command(name="blacklist")
    async def send_blacklist(self, ctx):
        """Don't answer commands in this channel"""

        if isinstance(ctx.channel, discord.TextChannel):
            if ctx.author.permissions_in(ctx.channel).manage_guild:
                
                if data.blacklist(ctx.guild.id, ctx.channel.id):
                    await ctx.send("Added this channel to my blacklist for this server. I won't answer commands in this channel anymore (except blacklist).")
                else:
                    await ctx.send("Removed this channel from my blacklist for this server.")

            else:
                if data.can_send_to_channel(ctx.guild.id, ctx.channel.id):
                    await ctx.send("This command requires you to have the \"manage server\" permission.")
        else:
            await ctx.send("I can only add text channels to the blacklist.")

    @commands.command(name="whitelist")
    async def send_whitelist(self, ctx):
        """Only answer commands in this channel"""

        if isinstance(ctx.channel, discord.TextChannel):
            if ctx.author.permissions_in(ctx.channel).manage_guild:
                
                if data.whitelist(ctx.guild.id, ctx.channel.id):
                    await ctx.send("Added this channel to my whitelist for this server. I will only answer commands in whitelisted channels.")
                else:
                    await ctx.send("Removed this channel from my whitelist for this server.")

            else:
                if data.can_send_to_channel(ctx.guild.id, ctx.channel.id):
                    await ctx.send("This command requires you to have the \"manage server\" permission.")
        else:
            await ctx.send("I can only add text channels to the whitelist.")

    # creator-restricted commands, only I can use these
    @commands.command(name="servers", hidden=True)
    @commands.is_owner()
    async def send_servers(self, ctx):
        servers = self.bot.guilds
        output = ""
        for server in servers:
            output = output + str(server.id) + " : " + server.name + "\n"
        await ctx.send(output)

    @commands.command(name="leaveServer", hidden=True)
    @commands.is_owner()
    async def send_leave(self, ctx, guildID=None):
        if not guildID:
            return
        try:
            targetGuild = self.bot.get_guild(id=int(guildID))
            await targetGuild.leave()
            await ctx.send("Successfully left server: " + targetGuild.name)
        except discord.errors.Forbidden:
            # we're in the server we're trying to leave, so we can't send the confirmation message
            # or for some reason we don't have permission to post in the channel
            pass
        except Exception as e:
            print(ctx.message.content + str(e))
            await ctx.send("Error: could not leave target server.")

def setup(bot):
    bot.add_cog(Meta_Cog(bot))