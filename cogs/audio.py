import discord
import youtube_dl
import asyncio

from discord.ext import commands

# local imports
from shared import *

# YTDLSource class and audio streaming functionality taken from https://github.com/Rapptz/discord.py/blob/master/examples/basic_voice.py

ytdl_format_options = {
    'format': 'bestaudio/best',
    'outtmpl': '%(extractor)s-%(id)s-%(title)s.%(ext)s',
    'restrictfilenames': True,
    'noplaylist': True,
    'nocheckcertificate': True,
    'ignoreerrors': False,
    'logtostderr': False,
    'quiet': True,
    'no_warnings': True,
    'default_search': 'auto',
    'source_address': '0.0.0.0' # bind to ipv4 since ipv6 addresses cause issues sometimes
}

ffmpeg_options = {
    'options': '-vn'
}

ytdl = youtube_dl.YoutubeDL(ytdl_format_options)

class YTDLSource(discord.PCMVolumeTransformer):
    def __init__(self, source, *, data=None, volume=0.5):
        super().__init__(source, volume)

        if(data):
            self.data = data

            self.title = data.get('title')
            self.url = data.get('url')

    @classmethod
    async def from_url(self, url, *, loop=None):
        loop = loop or asyncio.get_event_loop()
        data = await loop.run_in_executor(None, lambda: ytdl.extract_info(url, download=False))

        if 'entries' in data:
            # take first item from a playlist
            data = data['entries'][0]

        filename = data['url']
        return self(discord.FFmpegPCMAudio(filename, **ffmpeg_options), data=data)

    @classmethod
    def from_url_simple(self, url):
        """A simplified generator where we just use the URL as an audio source, for use with looping"""
        return self(discord.FFmpegPCMAudio(url, **ffmpeg_options))

class AudioManager:
    def __init__(self, bot, ctx):
        self.bot = bot
        self.serverID = ctx.guild.id
        self.ctx = ctx
        
        self.queue = []
        self.loop = False

        self.nowPlaying = {}
        self.player = None
        self.afterPlay = None

    async def play(self, url:str):
        try:
            self.player = await YTDLSource.from_url(url, loop=self.bot.loop)
        except Exception as error:
            if isinstance(error, youtube_dl.utils.DownloadError):
                await self.ctx.send("Could not load that video. It may be age-restricted or private.")
                return
            else:
                raise error

        if self.ctx.voice_client.is_playing() or self.ctx.voice_client.is_paused():
            self.enqueue(url)
            await self.ctx.send(f"`{ytdl.extract_info(url, download=False).get('title')}` added to queue")
            return

        def after_play(error):
            print(self.loop)
            if error:
                raise error
            if self.loop:
                self.bot.loop.create_task(self.play(url))
            else:
                self.play_next()

        self.nowPlaying = self.player.data
        self.afterPlay = after_play
        self.ctx.voice_client.play(self.player, after=self.afterPlay)
        
        await self.ctx.send(f"Now playing: `{self.player.title}`")

    def play_next(self):
        if self.queue:
            self.bot.loop.create_task(self.play(self.dequeue()))

    def skip(self):
        self.ctx.voice_client.stop()
        self.afterPlay = None
        self.play_next()

    def enqueue(self, audio:str):
        self.queue.append(audio)

    def dequeue(self) -> str:
        if self.queue:
            return self.queue.pop(0)
        else:
            return ""

    def get_next(self) -> str:
        if len(self.queue) > 0:
            return self.queue[0]
        else:
            return ""

    def get_loop(self) -> bool:
        return self.loop

    def set_loop(self, state:bool):
        self.loop = state

    def toggle_loop(self) -> bool:
        if self.loop:
            self.loop = False
            return False
        else:
            self.loop = True
            return True

    def reset(self):
        self.queue = []
        self.loop = False
        self.nowPlaying = {}
    
    def __str__(self):
        return f"serverID: {self.serverID}\nqueue: {self.queue}\nloop: {self.loop}\nnowPlaying: {self.nowPlaying.get('title')}"


audioManagers = {}

class Audio_Cog(commands.Cog, name="Audio"):
    def __init__(self, bot):
        self.bot = bot

    @commands.Cog.listener()
    async def on_ready(self):
        print("Audio cog ready")
    
    @commands.Cog.listener()
    async def on_voice_state_update(self, member, before, after):
        # if the event is the someone leaving a voice channel
        if before.channel and (before.channel != after.channel):

            # if the person leaving is the bot, reset audio data
            if member == self.bot.user:
                # we check the channel instead of member on the off-chance we were disconnected for a kick
                audioManagers[before.channel.guild.id].reset()

            # otherwise if we're the only one left in the channel, leave
            elif (self.bot.user in before.channel.members) and (len(before.channel.members) == 1):
                voice = discord.utils.get(self.bot.voice_clients, guild=before.channel.guild)
                await voice.disconnect()

    def check_for_manager(self, ctx):
        serverID = ctx.guild.id
        if serverID not in audioManagers:
            audioManagers[serverID] = AudioManager(self.bot, ctx)

    def check_role(self):
        pass

    async def check_in_voice(self, ctx):
        try:
            voiceChannel = ctx.author.voice.channel
            return True
        except AttributeError:
            await ctx.send("You need to be in a voice channel to do this")
            return False

    @commands.command(name="play")
    @commands.check(can_send)
    async def play(self, ctx, *, url):
        """Streams audio from a url"""
        serverID = ctx.guild.id
        self.check_for_manager(ctx)

        try:
            voiceChannel = ctx.author.voice.channel
        except AttributeError:
            await ctx.send("You need to be in a voice channel to do this")
            return

        try:
            await voiceChannel.connect()
        except discord.ClientException:
            # already connected
            pass

        await audioManagers[serverID].play(url)

    @commands.command(name="skip")
    @commands.check(can_send)
    async def skip(self, ctx):
        if not await self.check_in_voice(ctx):
            return
        serverID = ctx.guild.id
        self.check_for_manager(ctx)

        voice = ctx.voice_client
        
        if not(voice and (voice.is_playing() or voice.is_paused())):
            await ctx.send("Nothing is playing")
            return

        audioManagers[serverID].skip()

        await ctx.send("Skipping")

    @commands.command(name="loop")
    @commands.check(can_send)
    async def loop(self, ctx):
        if not await self.check_in_voice(ctx):
            return
        serverID = ctx.guild.id
        self.check_for_manager(ctx)
        if audioManagers[serverID].toggle_loop():
            await ctx.send("Enabled looping")
        else:
            await ctx.send("Disabled looping")

    @commands.command(name="leave")
    @commands.check(can_send)
    async def leave(self, ctx):
        if not await self.check_in_voice(ctx):
            return
        serverID = ctx.guild.id
        self.check_for_manager(ctx)
        voice = discord.utils.get(self.bot.voice_clients, guild=ctx.guild)
        if voice.is_connected():
            await voice.disconnect()
            audioManagers[serverID].reset()
            await ctx.send("Disconnecting")
        else:
            await ctx.send("The bot is not connected to a voice channel.")

    @commands.command(name="pause")
    @commands.check(can_send)
    async def pause(self, ctx):
        if not await self.check_in_voice(ctx):
            return
        serverID = ctx.guild.id
        self.check_for_manager(ctx)
        voice = discord.utils.get(self.bot.voice_clients, guild=ctx.guild)
        if voice.is_playing():
            voice.pause()
            await ctx.send("Pausing")
        else:
            await ctx.send("Currently no audio is playing.")

    @commands.command(name="resume")
    @commands.check(can_send)
    async def resume(self, ctx):
        if not await self.check_in_voice(ctx):
            return
        serverID = ctx.guild.id
        self.check_for_manager(ctx)
        voice = ctx.voice_client
        if voice.is_paused():
            voice.resume()
            await ctx.send("Resuming")
        else:
            await ctx.send("The audio is not paused.")

    @commands.command(name="stop")
    @commands.check(can_send)
    async def stop(self, ctx):
        if not await self.check_in_voice(ctx):
            return
        serverID = ctx.guild.id
        self.check_for_manager(ctx)
        audioManagers[serverID].reset()
        ctx.voice_client.stop()
        await ctx.send("Stopping audio and emptying queue")

    @commands.command(name="audio.debug", hidden=True)
    @commands.check(can_send)
    @commands.is_owner()
    async def audio_debug(self, ctx):
        serverID = ctx.guild.id
        self.check_for_manager(ctx)
        await ctx.send(audioManagers[serverID])

def setup(bot):
    bot.add_cog(Audio_Cog(bot))