from discord.ext import commands

from random import randint
import operator as op
import re

# local imports
from shared import *

class Roll:

    total, rolls, removed = 0, [], []

    def __init__(self, amount:int, size:int, keep:int=None, drop:int=None, explode:str="", floor:int=0, rerollFloor:int=0):
        def randomRoll():
            roll = 0
            # A do-while loop to make a roll until it's above the rerollFloor
            while True:
                roll = max(randint(1, size), floor)
                if(roll >= rerollFloor):
                    break
            return roll

        total, rolls, removed = 0, [], []
        explode = explode.lower()
        rollCounter = 0

        # keep amount and size within bounds
        amount = max(0, int(amount))
        size = max(1, int(size))

        while rollCounter < amount:
            rolls.append(randomRoll())

            if explode:
                if rolls[-1] == size:
                    # compound means each re-roll can trigger its own explosion
                    if explode == "c" or explode == "compound":
                        while rolls[-1] == size:
                            rolls.append(randomRoll())
                    # penetrating is like compound but the re-rolls have 1 subtracted (after checking for explosion)
                    elif explode == "p" or explode == "penetrating":
                        rolls.append(randomRoll()-1)
                        while rolls[-1] == size-1:
                            rolls.append(randomRoll()-1)
                    else:
                        rolls.append(randomRoll())
            rollCounter = rollCounter + 1

        # keep x means keep the x highest-value dice
        # it is mutually exclusive with drop, and takes priority
        if keep:
            keep = max(0, int(keep))
            for i in range(0, len(rolls) - keep):
                removed.append(min(rolls))
                rolls.remove(removed[-1])

        # drop x means remove the x lowest-value dice
        # it is mutually exclusive with keep
        elif drop:
            drop = max(0, int(drop))
            for i in range(0, drop):
                removed.append(min(rolls))
                rolls.remove(removed[-1])

        for roll in rolls:
            total = total + roll
        
        self.total = total
        self.rolls = rolls
        self.removed = removed

    # OVERLOADING

    def __getitem__(self, item):
        return self.rolls[item]

    def __iter__(self):
        # any iteration over the class can just use the list of rolls
        return self.rolls.__iter__()

    def __str__(self):
        output = " ("
        for roll in self.rolls:
            output = output + str(roll) + " + "
        
        output = output.rstrip(" +") + ")"

        if self.removed:
            output = output + " (removed: "
            for r in self.removed:
                output = output + str(r) + " + "
            output = output.rstrip(" +") + ")"

        return str(self.total) + output

    def __add__(self, value):
        return self.total + value

    def __radd__(self, value):
        return self.total + value

    def __sub__(self, value):
        return self.total - value

    def __rsub__(self, value):
        return self.total - value

    def __mul__(self, value):
        return self.total * value

    def __rmul__(self, value):
        return self.total * value

    def __truediv__(self, value):
        return self.total / value

    def __rtruediv__(self, value):
        return self.total / value

    def __floordiv__(self, value):
        return self.total // value

    def __rfloordiv__(self, value):
        return self.total // value

    def __mod__(self, value):
        return self.total % value

    def __rmod__(self, value):
        return self.total % value

    def __lt__(self, value):
        total = 0
        for roll in self.rolls:
            if roll < value:
                total = total + 1
        return total
    
    def __gt__(self, value):
        total = 0
        for roll in self.rolls:
            if roll > value:
                total = total + 1
        return total

    def __le__(self, value):
        total = 0
        for roll in self.rolls:
            if roll <= value:
                total = total + 1
        return total
    
    def __ge__(self, value):
        total = 0
        for roll in self.rolls:
            if roll >= value:
                total = total + 1
        return total

    def __eq__(self, value):
        total = 0
        for roll in self.rolls:
            if roll == value:
                total = total + 1
        return total

    def __ne__(self, value):
        total = 0
        for roll in self.rolls:
            if roll != value:
                total = total + 1
        return total

class RollParser:
    rollRegex = re.compile(r"([\d-]+)d([\d-]+)(?:k([\d-]+)|d([\d-]+))?(!p?)?")

    symbols = {
        '+':op.add, '-':op.sub, '*':op.mul, '/':op.truediv,
        '>':op.gt, '<':op.lt, '>=':op.ge, '<=':op.le, '=':op.eq
    }

    Symbol = str
    Number = (int, float)
    Atom = (Symbol, Number, bool)
    Exp = (Atom, list)

    total = 0
    rolls = []

    def __init__(self, expr:str):
        self.rolls = []
        parsedRoll = self.eval(self.read_from_tokens(self.tokenize(("(" + expr + ")"))))
        if isinstance(parsedRoll, Roll):
            self.total = parsedRoll.total
        else:
            self.total = parsedRoll

    def tokenize(self, chars):
        # needs support for equals
        return chars.replace(
            '(', ' ( ').replace(
            ')', ' ) ').replace(
            '+', ' + ').replace(
            '-', ' - ').replace(
            '*', ' * ').replace(
            '/', ' / ').replace(
            '>', ' > ').replace(
            '<', ' < ').replace(
            '> =', '>= ').replace(
            '< =', '<= ').split()

    def read_from_tokens(self, tokens: list) -> Exp:
        "Read an expression from a sequence of tokens."
        if len(tokens) == 0:
            raise SyntaxError('unexpected EOF')
        token = tokens.pop(0)
        if token == '(':
            L = []
            while tokens[0] != ')':
                L.append(self.read_from_tokens(tokens))
            tokens.pop(0) # pop off ')'
            return L
        elif token == ')':
            raise SyntaxError('unexpected )')
        else:
            return self.atom(token)

    def atom(self, token: str) -> Atom:
        "Numbers become numbers; every other token is a symbol."
        try: return int(token)
        except ValueError:
            try: return float(token)
            except ValueError:
                return self.Symbol(token)

    def eval(self, x: Exp) -> Exp:
        # print(x)
        if isinstance(x, list):
            values = []
            operator = ""
            if len(x) > 1:
                for y in x:
                    if isinstance(y, list):
                        y = self.eval(y)
                    
                    if y in self.symbols:
                        operator = y
                    else:
                        values.append(y)

                    if len(values) == 2:
                        #we need to reverse the list because it's a stack when we need a queue
                        #should just make it a queue later
                        values.reverse()
                        values.append(self.symbols[operator](self.eval(values.pop()), self.eval(values.pop())))
                return values[0]
            else:
                return self.eval(x[0])
        elif (m := self.rollRegex.match(str(x))):
            parsed = m.groups()
            explode = ""
            if parsed[4]:
                explode = {"!":"e", "!!":"c", "!p":"p"}[parsed[4]]
            myRoll = Roll(parsed[0], parsed[1], keep=parsed[2], drop=parsed[3], explode=explode)
            self.rolls.append([x, str(myRoll)])
            return myRoll
        else:
            return x


    def __str__(self):
        return str(self.rolls)

class Dice_Cog(commands.Cog, name="Dice"):
    def __init__(self, bot):
        self.bot = bot

    @commands.Cog.listener()
    async def on_ready(self):
        print("Dice cog ready")

    @commands.command(name="roll", aliases=["r"])
    @commands.check(can_send)
    async def send_roll(self, ctx, *, arg=None):
        """Read the input as a roll and send the result to the channel"""
        if not arg:
            return
        try:
            myRoll = RollParser(arg)
            output = ""
            for roll in myRoll.rolls:
                output = output + str(roll) + "\n"
            output = output + "Total: " + str(myRoll.total)
            await ctx.send(output)
        except TypeError:
            await ctx.send("Couldn't understand that expression")

    @commands.command(name="stats")
    @commands.check(can_send)
    async def send_stats(self, ctx, arg=None):
        """Roll a stat array"""
        if not arg:
            await ctx.send(
                "Which method would you like to use? Type `" + prefix + "stats x`, replacing x with a number:\n"
                + "\t1: Richard's method (4d6k3 seven times, highest 6, guaranteed at least one 16 and 15 or higher and one 8 or lower)\n"
                + "\t2: Forgiving/Nabil (4d6k3 seven times, highest 6)\n"
                + "\t3: PHB standard (4d6k3 six times)\n"
                + "\t4: Heroic/Colville (3d6 six times, two of which are guaranteed to be 16 or higher)\n"
                + "\t5: Brutal (3d6 six times, in order. Fuck you.)"
            )

        elif arg in ["1", "richard"]:
            stats = []
            for i in range(0,7):
                stats.append(Roll(4,6,keep=3).total)
            stats.sort(reverse=True)
            stats.pop()
            if min(stats) > 8:
                stats.pop()
                stats.append(8)
            if stats[1] < 15:
                stats[1] = 15
            if stats[0] < 16:
                stats[0] = 16
            await ctx.send("Your stats are: " + str(stats))

        elif arg in ["2", "forgiving", "nabil"]:
            stats = []
            for i in range(0,7):
                stats.append(Roll(4,6,keep=3).total)
            stats.sort(reverse=True)
            stats.pop()
            await ctx.send("Your stats are: " + str(stats))

        elif arg in ["3", "phb", "standard"]:
            stats = []
            for i in range(0,6):
                stats.append(Roll(4,6,keep=3).total)
            stats.sort(reverse=True)
            await ctx.send("Your stats are: " + str(stats))

        elif arg in ["4", "heroic", "colville"]:
            stats = []
            # rather than actually rolling until we get 16 or higher it's more computationally efficient
            # to just generate one number and use probability to determine whether we get 16, 17, or 18
            for i in range(0,2):
                rollChance = randint(1,10)
                if rollChance <= 6:
                    stats.append(16)
                elif rollChance <= 9:
                    stats.append(17)
                else:
                    stats.append(18)

            # and the rest we just roll normally
            for i in range(0, 4):
                stats.append(Roll(3,6).total)
            stats.sort(reverse=True)
            await ctx.send("Your stats are: " + str(stats))

        elif arg in ["5", "brutal", "fuck"]:
            stats = []
            for i in range(0,6):
                stats.append(Roll(3,6).total)
            await ctx.send("Your stats are: " + str(stats))

def setup(bot):
    bot.add_cog(Dice_Cog(bot))