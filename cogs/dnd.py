from random import choice

from discord.ext import commands

# local imports
from resources.tips import tips
from resources.wildMagic import wildMagic
from resources.wildMagicNL import wildMagicNL
from classes.conditions import conditions
from classes.rules import rules
from classes.feats import feats
from shared import *

class DND_Cog(commands.Cog, name="D&D"):
    def __init__(self, bot):
        self.bot = bot

    @commands.Cog.listener()
    async def on_ready(self):
        print("D&D cog ready")

    @commands.command(name="tip", aliases=["tips", "loading"])
    @commands.check(can_send)
    async def send_tip(self, ctx):
        """Send a random loading screen tip to the channel"""
        await ctx.send(choice(tips))

    @commands.command(name="surge", aliases=["wildmagic"])
    @commands.check(can_send)
    async def send_surge(self, ctx):
        """Roll a Wild Magic Surge"""
        await ctx.send(choice(wildMagic))

    @commands.command(name="netlibram", aliases=["surgenl", "wildmagicnl"])
    @commands.check(can_send)
    async def send_netlibram(self, ctx):
        """Roll on the Net Libram of Random Magical Effects"""
        await ctx.send(choice(wildMagicNL))

    @commands.command(name="condition", aliases=["conditions"])
    @commands.check(can_send)
    async def send_condition(self, ctx, arg=None):
        """Get information on a condition"""
        condition = conditions.search(arg)
        if condition:
            await send_long(ctx, condition)
        else:
            await ctx.send(f"Did not recognize that condition name. Use `{prefix}list conditions` to see what I recognize.")

    @commands.command(name="rule", aliases=["rules"])
    @commands.check(can_send)
    async def send_rule(self, ctx, *, arg=None):
        """Get information on a rule"""
        if arg == "34":
            await ctx.send(choice([
                "no",
                "https://www.youtube.com/watch?v=uBYXFiSrAaA",
                "https://cdn.discordapp.com/attachments/817949766939705374/822731887918317578/Screen_Shot_2020-04-28_at_12.21.48_PM.jpg",
            ]))
            return

        rule = rules.search(arg)
        if rule:
            await send_long(ctx, rule)
        else:
            # if the rule doesn't match, see if it matches a condition
            # I've seen some people try to $rule a condition
            condition = conditions.search(arg)
            if condition:
                await send_long(ctx, condition)
            else:
                await ctx.send(f"Did not recognize that rule name. Use `{prefix}list rules` to see what I recognize.")

    @commands.command(name="feat", aliases=["feats"])
    @commands.check(can_send)
    async def send_feat(self, ctx, *, arg=None):
        feat = feats.search(arg)
        if feat:
            await send_long(ctx, feat)
        else:
            await ctx.send(f"Did not recognize that feat name. Use `{prefix}list feats` to see what I recognize.")

    @commands.command(name="item")
    @commands.check(can_send)
    async def send_item(self, ctx, arg=None):
        return
        # await ctx.send("Command not yet implemented")

    # shortcuts for certain rules, not sure if I actually want to use them
    # @commands.command(name="grapple", aliases=["grappling"])
    # @commands.check(can_send)
    # async def send_grapple(ctx, rule=None):
    #     await send_rule(ctx, rule="grappling")

    # @commands.command(name="jump", aliases=["jumping"])
    # @commands.check(can_send)
    # async def send_jump(ctx, rule=None):
    #     await send_rule(ctx, rule="jumping")

    @commands.command(name="list")
    @commands.check(can_send)
    async def send_list(self, ctx, arg=None):
        """List all: conditions or rules"""
        if not arg:
            ctx.send("I can list my `conditions`, `rules`, or `feats`. Send the command again followed by one of these.")
        elif arg.startswith("condition"):
            await send_long(ctx, "I know the following conditions: " + conditions.list_conditions())
        elif arg.startswith("rule"):
            await send_long(ctx, "I know the following rules: " + rules.list_rules())
        elif arg.startswith("feat"):
            await send_long(ctx, "I know the following feats: " + feats.list_feats())

def setup(bot):
    bot.add_cog(DND_Cog(bot))