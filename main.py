from discord.ext import commands
import sys

# local imports
import auth
from shared import *
from classes.data import data

# Mr_Pie_5 = 206239684882464769

bot = commands.Bot(command_prefix=prefix)
debug = True if ("-debug" in sys.argv) else False

extensions = [
    'cogs.audio',
    'cogs.dice',
    'cogs.dnd',
    'cogs.eightball',
    'cogs.errors',
    'cogs.markov',
    'cogs.meta',
    'cogs.shitpost',
    'cogs.wtf',
]

@bot.event
async def on_ready():
    print(f"We have logged in as {bot.user}")
    print("Loading server data...")
    if data.load():
        print("Successfully loaded data")
    
    print("Updating server data...")
    for server in bot.guilds:
        data.initialize_server(server.id)
    print("Finished updating server data")

    print("Main ready")

@bot.event
async def on_guild_join(guild):
    data.initialize_server(guild.id)


@bot.command(name="unload", hidden=True)
@commands.is_owner()
async def unload(ctx, extension):
    try:
        await ctx.message.add_reaction("⌛")
        bot.unload_extension("cogs." + extension)
        await ctx.message.add_reaction("✅")
    except Exception as e:
        print(f"Failed to unload extension {extension}\n{type(e).__name__}: {e}")
        await ctx.message.add_reaction("🚫")

@bot.command(name="load", hidden=True)
@commands.is_owner()
async def load(ctx, extension):
    try:
        await ctx.message.add_reaction("⌛")
        bot.load_extension("cogs." + extension)
        await ctx.message.add_reaction("✅")
    except Exception as e:
        print(f"Failed to load extension {extension}\n{type(e).__name__}: {e}")
        await ctx.message.add_reaction("🚫")

@bot.command(name="reload", hidden=True)
@commands.is_owner()
async def reload(ctx, extension):
    try:
        await ctx.message.add_reaction("⌛")
        bot.reload_extension("cogs." + extension)
        await ctx.message.add_reaction("✅")
    except Exception as e:
        print(f"Failed to load cog {extension}\n{type(e).__name__}: {e}")
        await ctx.message.add_reaction("🚫")

@bot.command(name="load-all", hidden=True)
@commands.is_owner()
async def load_all(ctx):
    await ctx.message.add_reaction("⌛")
    for extension in extensions:
        try:
            bot.load_extension(extension)
        except commands.ExtensionAlreadyLoaded:
            try:
                bot.reload_extension(extension)
            except Exception as e:
                print(f"Failed to load cog {extension}\n{type(e).__name__}: {e}")
                await ctx.send(f"Could not load {extension}")
    await ctx.message.add_reaction("✅")

for extension in extensions:
    try:
        bot.load_extension(extension)
    except Exception as e:
        print(f"Failed to load extension {extension}\n{type(e).__name__}: {e}")


bot.run(auth.testToken if debug else auth.token)